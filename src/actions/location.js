import * as types from './types';

const getPrevPathSuccess = prevPath => ({
  type: types.GET_PREV_PATH_SUCCESS,
  payload: prevPath
});

const getPrevPathFailure = err => ({
  type: types.GET_PREV_PATH_FAILURE,
  err
});

export const getLocation = prevPath => {
  console.log(prevPath);
  return dispatch => {
    if (prevPath) {
      return dispatch(getPrevPathSuccess(prevPath));
    } else {
      dispatch(getPrevPathFailure(prevPath));
    }
  };
};

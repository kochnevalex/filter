import * as types from './types';
import config from '../config';
import axios from 'axios';


const getStartDataRequest = {
    type: types.GET_START_DATA__REQUEST,
    isFetching: false
};

const getStartDataSuccess = todos => ({
    type: types.GET_START_DATA_SECCESS,
    payload: todos
});

const getStartDataFailure = err => ({
    type: types.GET_START_DATA_FAILURE,
    err
});


export const getStartData = () => {
    return async dispatch => {
        try {
            dispatch(getStartDataRequest);

            const res = await axios({
                method: 'get',
                url: config.endpoint
            });
            if (res.data) {
                dispatch(getStartDataSuccess(res.data));
                return res.data;
            } else {
                //throw new ApiError(res.data);
            }
        } catch (e) {
            dispatch(getStartDataFailure(e.message));
            throw e;
        }
    };
};

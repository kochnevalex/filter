const config = {
    development: {
        //test server
        endpoint: 'https://jsonplaceholder.typicode.com/todos'
    },
    staging: {
        endpoint: ''
    },
    production: {
        endpoint: ''
    }
};
export default config[process.env.REACT_APP_CONFIG_ENV] || config.development;

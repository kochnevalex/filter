import { GET_PREV_PATH_SUCCESS, GET_PREV_PATH_FAILURE } from '../actions/types';

const initState = {
  prevPath: [],
  err: null
};

export default function(state = initState, action) {
  switch (action.type) {
    case GET_PREV_PATH_SUCCESS:
      return Object.assign({}, state, {
        prevPath: action.payload
      });
    case GET_PREV_PATH_FAILURE:
      return Object.assign({}, state, {
        err: action.err
      });
    default:
      return state;
  }
}

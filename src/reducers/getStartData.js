import { GET_START_DATA_SECCESS, GET_START_DATA_FAILURE, GET_START_DATA__REQUEST } from '../actions/types';

const initState = {
    todos: [],
    isFetchingPost: false,
    err: null
};

export default function(state = initState, action) {
    switch (action.type) {
        case GET_START_DATA__REQUEST:
            return Object.assign({}, state, {
                isFetchingPost: true
            });
        case GET_START_DATA_SECCESS:
            return Object.assign({}, state, {
                todos: action.payload
            });
        case GET_START_DATA_FAILURE:
            return Object.assign({}, state, {
                err: action.err
            });
        default:
            return state;
    }
}

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import location from './location';
import getStartData from './getStartData';

export default combineReducers({
  location,
  getStartData,
  router: routerReducer,
});
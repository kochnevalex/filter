import React, { Component } from 'react';
import {Form as PolarisForm} from '@shopify/polaris';



class Form extends Component {
    render() {
        return (
            <PolarisForm onSubmit={this.handleSubmit}>
                {this.props.children}
            </PolarisForm>
        );
    }
/*
    handleSubmit = (event) => {
        this.setState({newsletter: false, email: ''});
    };

    handleChange = (field) => {
        return (value) => this.setState({[field]: value});
    };
    */
}

export default Form;
import React, { Component } from 'react';
import {Checkbox as PolarisCheckbox} from '@shopify/polaris';


class Checkbox extends Component {
    state = {
        checked: false,
    };
    render() {
        const {checked} = this.state;

        return (
            <PolarisCheckbox
                checked={checked}
                label="Basic checkbox"
                onChange={this.handleChange}
            />
        );
    }

    handleChange = (value) => {
        this.setState({checked: value});
    };

}

export default Checkbox;

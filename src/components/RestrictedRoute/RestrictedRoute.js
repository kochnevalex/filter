import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Route } from 'react-router-dom';
import { getLocation } from '../../actions/location';

class RestrictedRoute extends Component {
    componentWillReceiveProps(nextProps) {
        if (nextProps.location !== this.props.location) {
            this.props.getLocation(this.props.location);
        }
    }

    render() {
        return <Route {...this.props} component={this.props.component} />;
    }
}

const mapDispatchToProps = dispatch => ({
    getLocation: bindActionCreators(getLocation, dispatch)
});

export default connect(() => {{}}, mapDispatchToProps)(RestrictedRoute)

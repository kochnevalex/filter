import React, { Component } from 'react';
import {Select as PolarisSelect} from '@shopify/polaris';

class Select extends Component {
    state = {
        selected: 'today',
    };

    handleChange = (newValue) => {
        this.setState({selected: newValue});
    };

    render() {
        console.log(this.props.options)
        return (
            <PolarisSelect
                label="Date range"
                options={this.props.options}
                onChange={this.handleChange}
                value={this.state.selected}
            />
        );
    }
}

export default Select;
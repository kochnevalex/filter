import React, {Component} from 'react';
import {
    AppProvider,
    PageActions,
    Page,
    Card,
    FormLayout,
    Button,
    Checkbox,
    Label,
    Select,
    TextField,
    DataTable,
    Form
} from '@shopify/polaris';
import * as PropTypes from 'prop-types';
// import { history } from '../../store';
// import {Link} from 'react-router-dom'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getStartData} from '../../actions/getStartData';
import FormItem from '../../components/FormItem/FormItem';
import axios from 'axios';

class Dashboard extends Component {
    async componentWillMount() {
        const todosItem = await  this.props.getStartData();
        this.setState({todos: todosItem})
    }
    static contextTypes = {
        easdk: PropTypes.object
    }
    state = {
        active: false,
        filterOptions: {
            1: {
                optionName: {
                    label: 'Label',
                    value: 'Collection',
                    errorMessage: ''
                },
                optionType: {
                    label: 'Type',
                    selected: 'collection',
                    value: [
                        {
                            label: 'Tag',
                            value: 'tag',
                            disabled: false
                        },
                        {
                            label: 'Collection',
                            value: 'collection',
                            disabled: false
                        },
                        {
                            label: 'Price',
                            value: 'price',
                            disabled: false
                        },
                        {
                            label: 'Vendor',
                            value: 'vendor',
                            disabled: false
                        }
                    ]
                },
                optionStyle: {
                    label: 'Style',
                    selected: 'link',
                    value: [
                        {
                            label: 'Checkbox',
                            value: 'checkbox',
                            disabled: false
                        },
                        {
                            label: 'Link',
                            value: 'link',
                            disabled: false
                        },
                        {
                            label: 'Slider',
                            value: 'slider',
                            disabled: false
                        },
                        {
                            label: 'Radio',
                            value: 'radio',
                            disabled: false
                        },
                        {
                            label: 'Swatch',
                            value: 'swatch',
                            disabled: false
                        }
                    ]
                }
            },
            2: {
                optionName: {
                    label: 'Label',
                    value: 'Tag',
                    errorMessage: ''
                },
                optionType: {
                    label: 'Type',
                    selected: 'tag',
                    value: [
                        {
                            label: 'Tag',
                            value: 'tag',
                            disabled: false
                        },
                        {
                            label: 'Collection',
                            value: 'collection',
                            disabled: false
                        },
                        {
                            label: 'Price',
                            value: 'price',
                            disabled: false
                        },
                        {
                            label: 'Vendor',
                            value: 'vendor',
                            disabled: false
                        }
                    ]

                },
                optionStyle: {
                    label: 'Style',
                    selected: 'checkbox',
                    value: [
                        {
                            label: 'Checkbox',
                            value: 'checkbox',
                            disabled: false
                        },
                        {
                            label: 'Link',
                            value: 'link',
                            disabled: true
                        },
                        {
                            label: 'Slider',
                            value: 'slider',
                            disabled: true
                        },
                        {
                            label: 'Radio',
                            value: 'radio',
                            disabled: false
                        },
                        {
                            label: 'Swatch',
                            value: 'swatch',
                            disabled: false
                        }
                    ]
                }
            }
        },
        todos: [],
    };


    render() {
        const {active} = this.state;
        const {todos} = this.state;
        const {filterOptions} = this.state;


        const rows = [];

        Object.keys(filterOptions).map((key) =>{
            rows.push([
                key,
                <TextField
                    error={filterOptions[key].optionName.errorMessage}
                    name="email"
                    //label={filterOptions[key].optionName.label}
                    type="text"
                    value={filterOptions[key].optionName.value}
                    onChange={(value) => this.inputChange(value, key)}
                />,
                <Select
                disabled
                //label={filterOptions[key].optionType.label}
                options={filterOptions[key].optionType.value}
                value={filterOptions[key].optionType.selected}
                />,
                <Select
                    disabled={filterOptions[key].optionType.selected === 'price' ? true : (filterOptions[key].optionType.selected === 'collection' ? true : false)}
                    //label={filterOptions[key].optionStyle.label}
                    options={filterOptions[key].optionStyle.value}
                    value={filterOptions[key].optionStyle.selected}
                    onChange={(value) => this.selectChange(value, key)}
                />,
            <Button icon='delete'
                    onClick={() => this.handleDelete(key)}>delete</Button>
            ])
        })


        // let obj = {
        //     1: 'green',
        //         2: 'red'
        // }
        // console.log({...obj,2: 'blue'});
        return (
                <Page
                    title="MBC Product Filter"
                    separator
                    fullWidth
                    secondaryActions={[{content: 'Upgrade plan'}, {content: 'Navigation'}]}
                    actionGroups={[
                        {
                            title: 'Help',
                            actions: [
                                {
                                    content: 'Documentation',
                                    onAction: this.performFacebookShare,
                                },
                                {
                                    content: 'FAQs',
                                    onAction: this.performPinterestShare,
                                },
                                {
                                    content: ' Contact Us',
                                    onAction: this.performPinterestShare,
                                }
                            ],
                        },
                    ]}
                >
                    <ul>
                        {
                            todos.map((item, index)=> <li onClick={() => this.changeTitle(item.id)} key={index} data-id={item.id}  className={item.completed ? 'completed' : 'continue'} > {item.title} </li>)
                        }
                    </ul>


                    <Form onSubmit={this.handleSubmit}>
                        <Card sectioned title="Options"
                              actions={[{
                                  content: (<Button icon='add'> Add filter option </Button>),
                                  onAction: this.modalHandle
                              }]}>
                            <Card.Section>
                                <DataTable
                                    columnContentTypes={[
                                        'numeric',
                                        React.ReactNode,
                                        React.ReactNode,
                                        React.ReactNode,
                                        React.ReactNode,
                                    ]}
                                    headings={[
                                        'id',
                                        'Label',
                                        'Type',
                                        'Style',
                                        ' ',
                                    ]}
                                    rows={rows}
                                />
                            </Card.Section>
                            <PageActions
                                primaryAction={{
                                    content: 'Save',
                                    onAction: this.handleSubmit
                                }}
                            />
                        </Card>
                    </Form>

                    <FormItem addFormOptions={this.addFormOptions} active={active} modalHandle={this.modalHandle}/>
                </Page>
        );
    }

    addFormOptions = (newOptions) => {
        console.log(newOptions.errorMessage)
        // if (!newOptions.optionName.value || newOptions.optionName.value.len )
        //     return false;
        //if ()
        let newId = Object.keys(this.state.filterOptions).length + 1;

        this.state.filterOptions[newId] = (newOptions);
        this.modalHandle();
    }
    handleSubmit = (event) => {
        event.preventDefault();
        console.log('save')
        axios({
            method: 'post',
            url: 'https://shopify.makebecool.pro/mbcFilterApp/public/save',
            data: this.state.filterOptions
        });
    };

    inputChange = (value, key) => {
        if (!value || value.length < 3) {
            this.state.filterOptions[key].optionName.errorMessage = 'Enter 3 or more characters for filter option';
        } else {
            this.state.filterOptions[key].optionName.errorMessage = '';
        }
        this.state.filterOptions[key].optionName.value = value;
        this.setState({filterOptions: this.state.filterOptions});
        //this.setState({filterOptions[key]: {...this.state.filterOptions[key].optionName, value}})
        // this.setState({
        //     optionName: {
        //         value: value
        //     }
        // })
        // console.log(this.state.optionName)
    };

    selectChange = (value, key) => {
        this.state.filterOptions[key].optionStyle.selected = value;
        this.setState({filterOptions: this.state.filterOptions});
    };
    modalHandle = () => {
        this.setState(({active}) => ({active: !active}));
    };

    changeTitle = (id) => {
        const newTodos = this.props.todos.reduce((acc, current) => {
            if (current.id === id) {
                current.completed = !current.completed;
            }
            acc.push(current);
            return acc;
        }, []);
        this.setState({todos: newTodos})
    }
    handleDelete = (optionItem) => {
        delete this.state.filterOptions[optionItem];
        let newFilterOptions = {};
        Object.keys(this.state.filterOptions).map((key, index) => {
            newFilterOptions[index + 1] = this.state.filterOptions[key];
        });
        // const newOptions = this.state.filterOptions.reduce((acc, current, currentIndex) => {
        //     if (currentIndex === index) {
        //         console.log(index, current)
        //     } else {
        //         acc.push(current);
        //     }
        //     return acc;
        // }, []);
        this.setState({filterOptions: newFilterOptions});
    }
}

const mapStateToProps = state => ({
    isFetching: state.getStartData.isFetching,
    todos: state.getStartData.todos
});

const mapDispatchToProps = dispatch => ({
    getStartData: bindActionCreators(getStartData, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)


//export default Dashboard;


/*
                                   Object.keys(filterOptions).map((key) =>

                                <Card.Section>
                                    <FormLayout>
                                        <FormLayout.Group>
                                            <TextField
                                                error={filterOptions[key].optionName.errorMessage}
                                                name="email"
                                                label={filterOptions[key].optionName.label}
                                                type="text"
                                                value={filterOptions[key].optionName.value}
                                                onChange={(value) => this.inputChange(value, key)}
                                            />
                                            <Select
                                                disabled
                                                label={filterOptions[key].optionType.label}
                                                options={filterOptions[key].optionType.value}
                                                value={filterOptions[key].optionType.selected}
                                            />
                                            <Select
                                                disabled={filterOptions[key].optionType.selected === 'price' ? true : (filterOptions[key].optionType.selected === 'collection' ? true : false)}
                                                label={filterOptions[key].optionStyle.label}
                                                options={filterOptions[key].optionStyle.value}
                                                value={filterOptions[key].optionStyle.selected}
                                                onChange={(value) => this.selectChange(value, key)}
                                            />
                                            <div>
                                                <div className="Polaris-Labelled__LabelWrapper">
                                                    <Label>&#8195;</Label>
                                                </div>
                                                <Button icon='delete'
                                                        onClick={() => this.handleDelete(key)}>delete</Button>
                                            </div>

                                        </FormLayout.Group>
                                    </FormLayout>
                                </Card.Section>

                                )}
                                       */
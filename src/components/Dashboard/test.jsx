import React, {Component} from 'react'

class CheckList extends Component {
    state = {
        items: {
            item1: {checked: false, text: 'One checkbox'},
            item2: {checked: false, text: 'Another checkbox'},
            item3: {checked: false, text: 'More checkbox'},
            item4: {checked: false, text: 'Fourth checkbox'}
        }
    }

    toggleListItem = id => {
        const currentItem = this.state.items[id]
        this.setState({
            ...this.state,
            items: {
                ...this.state.items,
                [id]: {...currentItem, checked: !currentItem.checked}
            }
        })
    }

    renderListItem = (id, data) => {
        const toggleCheckBox = () => this.toggleListItem(id)
        return <li key={id} onClick={toggleCheckBox}>{data.checked ? `!!! ${data.text} !!!` : data.text}</li>
    }

    render () {
        return (
            <ul>
                {Object.keys(this.state.items).map(id => this.renderListItem(id, this.state.items[id]))}
            </ul>
        )
    }
}

export default CheckList

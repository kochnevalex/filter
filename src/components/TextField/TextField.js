import React, { Component } from 'react';
import {TextField as PolarisTextField} from '@shopify/polaris';


class TextField extends Component {
    state = {
        value: 'Jaded Pixel',
    };

    handleChange = (value) => {
        this.setState({value});
    };

    render() {
        return (
            <PolarisTextField
                label="Store name"
                value={this.state.value}
                onChange={this.handleChange}
            />
        );
    }

    handleChange = (value) => {
        this.setState({value});
    };
}

export default TextField;
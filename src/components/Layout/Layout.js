import React, {Component} from 'react';
import {Switch} from 'react-router-dom';
import Dashboard from '../Dashboard/Dashboard';
import Login from '../Login/Login';
import RestrictedRoute from '../RestrictedRoute/RestrictedRoute';
import store from "../../store";
import {AppProvider} from "@shopify/polaris";
const shop = new URLSearchParams(window.location.search).get('shop')
const shopOrigin = shop ? `https://${shop}` : 'https://novinki-top.myshopify.com'
const apiKey =
    new URLSearchParams(window.location.search).get('apiKey') || 'de17afe08f5a358a2637d947f3cdb78f'

class Layout extends Component {
    render() {
        return (
            <AppProvider
                // apiKey={apiKey}
                // shopOrigin={shopOrigin}
                // forceRedirect={false}
            >
            <Switch>
                <RestrictedRoute exact path={'/login'} component={Login}/>
                <RestrictedRoute exact path={'/dashboard'} component={Dashboard}/>
            </Switch>
            </AppProvider>
        );
    }
}

export default Layout;

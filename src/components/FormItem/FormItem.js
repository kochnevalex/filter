import React, {Component} from 'react';
import {FormLayout, TextField, Select, Modal, InlineError, Button, ButtonGroup} from '@shopify/polaris';
class FormItem extends Component {
    state = {
        optionName: {
            label: 'Label',
            value: '',
            name: 'optionName',
            errorMessage: ''
        },
        optionType: {
            label: 'Type',
            selected: 'tag',
            name: 'optionType',
            value: [
                {
                    label: 'Tag',
                    value: 'tag',
                    disabled: false
                },
                {
                    label: 'Collection',
                    value: 'collection',
                    disabled: false
                },
                {
                    label: 'Price',
                    value: 'price',
                    disabled: false
                },
                {
                    label: 'Vendor',
                    value: 'vendor',
                    disabled: false
                }
            ]
        },
        optionStyle: {
            label: 'Style',
            selected: 'checkbox',
            name: 'optionStyle',
            value: [
                {
                    label: 'Checkbox',
                    value: 'checkbox',
                    disabled: false
                },
                {
                    label: 'Link',
                    value: 'link',
                    disabled: false
                },
                {
                    label: 'Slider',
                    value: 'slider',
                    disabled: false
                },
                {
                    label: 'Radio',
                    value: 'radio',
                    disabled: false
                },
                {
                    label: 'Swatch',
                    value: 'swatch',
                    disabled: false
                }
            ]
        }
    }

    componentWillMount() {
        if (this.state.optionType.selected === 'price') {
            this.setState({optionStyle: {...this.state.optionStyle, selected: 'slider'}})
        } else if (this.state.optionType.selected === 'collection') {
            this.setState({optionStyle: {...this.state.optionStyle, selected: 'link'}})
        } else if (this.state.optionType.selected === 'tag' || this.state.optionType.selected === 'vendor') {
            const newStyleValue = this.state.optionStyle.value.reduce((acc, current) => {
                if (current.value === 'slider' || current.value === 'link') {
                    current.disabled = true;
                }
                acc.push(current);
                return acc;
            }, []);
            this.setState({optionStyle: {...this.state.optionStyle, value: newStyleValue}})
            if (this.state.optionStyle.selected === 'slider' || this.state.optionStyle.selected === 'link') {
                this.setState({optionStyle: {...this.state.optionStyle, selected: 'checkbox'}});
            }
        }
    }

    render() {
        const {value: labelValue} = this.state.optionName;
        const {errorMessage} = this.state.optionName;
        const actions = [
                <Button
                    primary={false}
                    onClick={() => this.modalClose(this.props.modalHandle)}
                >Cancel</Button>,
            '   ',
                <Button
                    primary={true}
                    onClick={() => this.modalAdd(this.props.addFormOptions)}
                >Add</Button>


        ];
        return (

            <Modal
                open={this.props.active}
                onClose={() => this.modalClose(this.props.modalHandle)}
                primaryAction={{
                    content: 'Add',
                    onAction: () => this.modalAdd(this.props.addFormOptions),
                }}
                secondaryActions={[
                    {
                        content: 'Cancel',
                        onAction: () => this.modalClose(this.props.modalHandle)
                    },
                ]}
            >
                <Modal.Section>
                    <FormLayout>
                        <FormLayout>
                            <TextField error={errorMessage} name='optionName'
                                       label={this.state.optionName.label} value={labelValue} onChange={this.textChange}
                                       placeholder='Enter option label'/>
                            <Select name={this.state.optionType.name}
                                    label={this.state.optionType.label}
                                    options={this.state.optionType.value}
                                    value={this.state.optionType.selected}
                                    onChange={(value) => this.selectChange(value, this.state.optionType.name)}/>

                            <Select
                                disabled={this.state.optionType.selected === 'price' ? true : (this.state.optionType.selected === 'collection' ? true : false)}
                                name={this.state.optionStyle.name}
                                label={this.state.optionStyle.label}
                                options={this.state.optionStyle.value}
                                value={this.state.optionStyle.selected}
                                onChange={(value) => this.selectChange(value, this.state.optionStyle.name)}/>
                        </FormLayout>
                    </FormLayout>
                </Modal.Section>
            </Modal>

        );
    }

    styleValidator = () => {
        if (this.state.optionType.selected === 'price') {
            this.setState({optionStyle: {...this.state.optionStyle, selected: 'slider'}})
        } else if (this.state.optionType.selected === 'collection') {
            this.setState({optionStyle: {...this.state.optionStyle, selected: 'link'}})
        } else if (this.state.optionType.selected === 'tag' || this.state.optionType.selected === 'vendor') {
            const newStyleValue = this.state.optionStyle.value.reduce((acc, current) => {
                if (current.value === 'slider' || current.value === 'link') {
                    current.disabled = true;
                }
                acc.push(current);
                return acc;
            }, []);
            this.setState({optionStyle: {...this.state.optionStyle, value: newStyleValue}});
            if (this.state.optionStyle.selected === 'slider' || this.state.optionStyle.selected === 'link') {
                this.setState({optionStyle: {...this.state.optionStyle, selected: 'checkbox'}});
            }
        }
    };
    textChange = (value) => {
        if (!value || value.length < 3) {
            this.setState({
                optionName: {...this.state.optionName, value: value, errorMessage: 'Enter 3 or more characters for filter option'}
            })
        } else {
            this.setState({
                optionName: {...this.state.optionName, value: value, errorMessage: ''}
            })
        }
    };

    selectChange = (value, name) => {
        this.setState({[name]: {...this.state[name], selected: value}}, this.styleValidator)
    };

    modalAdd = (add) => {

        if (!this.state.optionName.value || this.state.optionName.value.length < 3) {
            this.setState({
                optionName: {...this.state.optionName, errorMessage: 'Enter 3 or more characters for filter option'}
            });
            return false;
        } else {
            this.setState({
                optionName: {...this.state.optionName, errorMessage: '',value: ''},optionType: {...this.state.optionType, selected: 'tag'},optionStyle: {...this.state.optionStyle, selected: 'checkbox'}
            });
            add(this.state);
        }
    };
    modalClose = (close) => {
        // this.setState({
        //     errorMessage: '', optionName: {...this.state.optionName, value: ''}
        // });
        close();
    };

    isInvalid = (content) => {
        if (!content) {
            return true;
        }

        return content.length < 3;
    };


    // handleSubmit = (event) => {
    //     console.log('sadas');
    // };


}

export default FormItem;


/*
<Modal
                open={this.props.active}
                onClose={() => this.modalClose(this.props.modalHandle)}
                primaryAction={{
                    content: 'Add',
                    onAction: () => this.modalAdd(this.props.addFormOptions),
                }}
                secondaryActions={[
                    {
                        content: 'Cancel',
                        onAction: () => this.modalClose(this.props.modalHandle)
                    },
                ]}
            >
                <Modal.Section>
                    <FormLayout>
                        <FormLayout>
                            <TextField error={errorMessage} name='optionName'
                                       label={this.state.optionName.label} value={labelValue} onChange={this.textChange}
                                       placeholder='Enter option label'/>
                            <Select name={this.state.optionType.name}
                                    label={this.state.optionType.label}
                                    options={this.state.optionType.value}
                                    value={this.state.optionType.selected}
                                    onChange={(value) => this.selectChange(value, this.state.optionType.name)}/>

                            <Select
                                disabled={this.state.optionType.selected === 'price' ? true : (this.state.optionType.selected === 'collection' ? true : false)}
                                name={this.state.optionStyle.name}
                                label={this.state.optionStyle.label}
                                options={this.state.optionStyle.value}
                                value={this.state.optionStyle.selected}
                                onChange={(value) => this.selectChange(value, this.state.optionStyle.name)}/>
                        </FormLayout>
                    </FormLayout>
                </Modal.Section>
            </Modal>
 */